select  instrument.instrument_name,
		A.quantity as sell_quantity,
        B.quantity as buy_quantity
	from
	(select deal_instrument_id,
			deal_type,
            sum(deal_quantity) as quantity
	from deal 
	group by deal_instrument_id, deal_type) A
    inner join
	(select deal_instrument_id,
			deal_type,
			sum(deal_quantity) as quantity
	from deal 
	group by deal_instrument_id, deal_type) B 
    inner join instrument
    
where A.deal_instrument_id = B.deal_instrument_id
      and A.deal_type = "S" and B.deal_type = "B"
      and instrument.instrument_id = A.deal_instrument_id; 