select 	instrument.instrument_name,
	    avg_sell_price, 
        avg_buy_price
from
(select 
	    A.deal_instrument_id,
        A.avg_price as avg_sell_price, 
        B.avg_price as avg_buy_price
	from
	(select 
			deal_instrument_id,
			deal_type,

            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price
	from deal 
	group by deal_instrument_id, deal_type) A,
    
	(select 
			deal_instrument_id,
			deal_type,
            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price
	from deal 
	group by  deal_instrument_id, deal_type) B 
    
where A.deal_instrument_id = B.deal_instrument_id
      and A.deal_type = "S" and B.deal_type = "B" ) T
      inner join instrument
where instrument.instrument_id = T.deal_instrument_id
