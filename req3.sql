select 	counterparty.counterparty_name,
	    net_trades_money
from
(select A.deal_counterparty_id, 
		(A.money - B.money) as net_trades_money
	from
	(select deal_counterparty_id,
			deal_type,
			sum(deal_amount * deal_quantity) as money
	from deal 
	group by deal_counterparty_id, deal_type) A,
    
	(select deal_counterparty_id,
			deal_type,
			sum(deal_amount * deal_quantity) as money
	from deal 
	group by deal_counterparty_id, deal_type) B 
    
where A.deal_counterparty_id = B.deal_counterparty_id 
      and A.deal_type = "S" and B.deal_type = "B") T
inner join counterparty
where counterparty.counterparty_id = T.deal_counterparty_id