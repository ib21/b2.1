use db_grad;

select  counterparty.counterparty_name, 
	    instrument.instrument_name,
		MainTab.net_trades_money,
		MainTab.net_trades_quantity,
        MainTab.avg_sell_price, 
        MainTab.avg_buy_price,
        Price_tab.last_sell_price,
        Price_tab.last_buy_price,
        case 
			when MainTab.net_trades_quantity < 0 then
            MainTab.net_trades_money - 
            MainTab.net_trades_quantity * (Price_tab.last_sell_price - MainTab.avg_buy_price)
            else 
            MainTab.net_trades_money + 
            MainTab.net_trades_quantity * (Price_tab.last_sell_price - MainTab.avg_buy_price)
		end as profit
from
(select A.deal_counterparty_id, 
	    A.deal_instrument_id,
		(A.money - B.money) as net_trades_money,
		(A.quantity - B.quantity) as net_trades_quantity,
        A.avg_price as avg_sell_price, 
        B.avg_price as avg_buy_price
	from
	(select deal_counterparty_id,
			deal_instrument_id,
			deal_type,
			sum(deal_amount * deal_quantity) as money,
            sum(deal_quantity) as quantity,
            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price
	from deal 
	group by deal_counterparty_id, deal_instrument_id, deal_type) A,
    
	(select deal_counterparty_id,
			deal_instrument_id,
			deal_type,
			sum(deal_amount * deal_quantity) as money,
            sum(deal_quantity) as quantity,
            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price
	from deal 
	group by deal_counterparty_id, deal_instrument_id, deal_type) B 
    
where A.deal_counterparty_id = B.deal_counterparty_id 
	  and A.deal_instrument_id = B.deal_instrument_id
      and A.deal_type = "S" and B.deal_type = "B") MainTab
      
inner join

(select Price.deal_counterparty_id,
	   Price.deal_instrument_id,
	   Price.deal_type,
	   Price.last_time,
       Price.last_price as last_sell_price,
       Price2.last_price as last_buy_price
from
(select LastDate.deal_counterparty_id,
	   LastDate.deal_instrument_id,
	   LastDate.deal_type,
	   LastDate.last_time,
       deal_amount as last_price
from
(select  deal_counterparty_id
		, deal_instrument_id
		, deal_type
        , max(deal_time) as last_time
	from deal  
    group by deal_counterparty_id, deal_instrument_id, deal_type) LastDate
inner join deal
where LastDate.deal_counterparty_id = deal.deal_counterparty_id 
	  and LastDate.deal_instrument_id = deal.deal_instrument_id
      and LastDate.deal_type = deal.deal_type
      and last_time = deal.deal_time
) Price,
(select LastDate.deal_counterparty_id,
	   LastDate.deal_instrument_id,
	   LastDate.deal_type,
	   LastDate.last_time,
       deal_amount as last_price
from
(select  deal_counterparty_id
		, deal_instrument_id
		, deal_type
        , max(deal_time) as last_time
	from deal  
    group by deal_counterparty_id, deal_instrument_id, deal_type) LastDate
inner join deal
where LastDate.deal_counterparty_id = deal.deal_counterparty_id 
	  and LastDate.deal_instrument_id = deal.deal_instrument_id
      and LastDate.deal_type = deal.deal_type
      and last_time = deal.deal_time
) Price2

where Price.deal_counterparty_id = Price2.deal_counterparty_id 
	  and Price.deal_instrument_id = Price2.deal_instrument_id
      and Price.deal_type = "S" and Price2.deal_type = "B") Price_tab

inner join counterparty inner join instrument

where MainTab.deal_counterparty_id = Price_tab.deal_counterparty_id 
	  and MainTab.deal_instrument_id = Price_tab.deal_instrument_id
      and MainTab.deal_counterparty_id = counterparty.counterparty_id
      and MainTab.deal_instrument_id = instrument.instrument_id
      
 

